datafusion
luigi
pyarrow
python-magic
pyzstd
tqdm
scancode-toolkit==31.2.5  # output may vary across versions; so we pin it in order to have reproducible datasets
